#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "app_list.h"

#define MAX_COMMANDS_LENGTH 100

struct app_list_el_t
{
	struct app_list_el_t *previous;
	char *name;
};


static struct app_list_el_t *previous_app = NULL;


bool app_list_init(){

	//Open list of commands for reading
	FILE *fp =  NULL;
	fp = popen("echo \"compgen -c\" | /bin/bash", "r");
	if(fp == NULL){
		LOG_FATAL("Could not execute compgen to get the list available commands in the system!");
		return false;
	}
	
	char curr_prgm[MAX_COMMANDS_LENGTH];
	while(fgets(curr_prgm, MAX_COMMANDS_LENGTH-1, fp)){
		
		//First, make sure the string is terminated
		curr_prgm[MAX_COMMANDS_LENGTH-1] = '\0';

		//Remove line break, if required
		char *newline = strchr(curr_prgm, '\n');
		if(newline != NULL)
			*newline = '\0';

		//Add the programm to the list
		struct app_list_el_t *programm;
		MALLOC_OR_FATAL(programm, sizeof(struct app_list_el_t));
		programm->previous = previous_app;
		previous_app = programm;

		//Copy programm name
		MALLOC_OR_FATAL(programm->name, sizeof(char)*(strlen(curr_prgm) + 1));
		strcpy(programm->name, curr_prgm);
	}

	//Close connection
	fclose(fp);

	//Success
	return true;

}

void app_list_search(const char *query, char** results, int max_results, int *number_results){
	
	*number_results = 0;
	struct app_list_el_t *curr_entry = previous_app;

	while(*number_results < max_results && curr_entry != NULL){
		
		if(strstr(curr_entry->name, query) != NULL){
			results[*number_results] = curr_entry->name;
			*number_results += 1;
		}

		curr_entry = curr_entry->previous;
	}
}

void app_list_append_to_list_store(GtkListStore *store){
	
	struct app_list_el_t *curr_entry = previous_app;
	GtkTreeIter iter;

	while(curr_entry != NULL){

		//Create a new entry in the row and append app name into it
		gtk_list_store_append(store, &iter);
		gtk_list_store_set(store, &iter, 0, curr_entry->name, -1);

		curr_entry = curr_entry->previous;
	}
}

void app_list_free(){

	struct app_list_el_t *curr;

	while(previous_app != NULL){
		curr = previous_app;
		previous_app = previous_app->previous;
		free(curr->name);
		free(curr);
	}

}
