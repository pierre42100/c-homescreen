#include <stdio.h>

#include "config.h"
#include "global.h"
#include "sys_helper.h"


int sys_helper_get_battery(){

	//Open file
	FILE *file = fopen(BATTERY_CHARGE_LOCATION, "r");

	//Check if file could not be opened
	if(file == NULL){
		LOG_ERROR("Could not open file to get battery charge level!");
		return -1;
	}

	//Get battery charge status
	int charge;
	fscanf(file, "%d", &charge);

	fclose(file);

	return charge;
}