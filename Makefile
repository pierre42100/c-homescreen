CC=gcc
CFLAGS= -Wall `pkg-config --cflags gtk+-3.0`
LIBS = `pkg-config --libs gtk+-3.0` -lpthread
DEPS = 
OBJ = main.o ui.o app_list.o background_thread.o sys_helper.o edt_parser.o ui_lock.o

all: clean homescreen

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

homescreen: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

run: clean homescreen
	./homescreen

clean:
	rm -f *.o homescreen
