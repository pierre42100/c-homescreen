/**
 * UI public functions
 *
 * @author Pierre HUBERT
 */

#include <gtk/gtk.h>
#include <time.h>

#include "global.h"
#include "edt_parser.h"

/**
 * UI data to display list
 */
struct ui_data_t {
	time_t time; //Current time
	int battery_charge;
};

/**
 * Initialize UI
 *
 * @param app The application to use to initialize app
 * @return TRUE in case of success / FALSE else
 */
bool ui_init(GtkApplication *app);

/**
 * Apply new data to the UI
 *
 * @param data The data to apply
 */
void ui_update_data(const struct ui_data_t *data);

/**
 * Display calendar events list
 *
 * @param firstEvent Pointer on the first event to show
 */
void ui_show_calendar(struct edt_parser_event_t * const firstEvent);