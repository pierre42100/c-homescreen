#include "ui_lock.h"
#include "global.h"

static GtkWidget *window;

static gboolean user_close_event(GtkWidget *widget, GdkEvent *event, gpointer data);
static void user_submit_event(GtkButton *button, gpointer data);

void ui_lock_show(){

	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(window), "Screen locked.");
	gtk_window_set_default_size(GTK_WINDOW(window), 200, 40);

	//Prevent the user from closing the window
	g_signal_connect(G_OBJECT(window), "delete-event", G_CALLBACK(user_close_event), NULL);

	//Add login form
	GtkWidget *box;
	box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 1);
	gtk_container_add(GTK_CONTAINER(window), box);

	//Password input;
	GtkWidget *password;
	password = gtk_entry_new();
	gtk_entry_set_placeholder_text(GTK_ENTRY(password), "User password");
	gtk_container_add(GTK_CONTAINER(box), password);


	//Submit button
	GtkWidget *button;
	button = gtk_button_new();
	gtk_button_set_label(GTK_BUTTON(button), "Login");
	g_signal_connect(GTK_BUTTON(button), "clicked", G_CALLBACK(user_submit_event), NULL);
	gtk_container_add(GTK_CONTAINER(box), button);


	gtk_widget_show_all(window);

}

//TODO : Remove when PAM is connected
static bool close_window = false;

static gboolean user_close_event(GtkWidget *widget, GdkEvent *event, gpointer data) {
	return close_window ? FALSE : TRUE; //TRUE = don't close
}

static void user_submit_event(GtkButton *button, gpointer data) {
	gtk_window_close(GTK_WINDOW(window));
	close_window = true;
}