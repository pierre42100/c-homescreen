/**
 * Background refresh thread
 *
 * @author Pierre HUBERT
 */

#include "global.h"

/**
 * Start background refresh thread
 *
 * @return bool TRUE for a success / FALSE else
 */
bool background_thread_start();