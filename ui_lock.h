/**
 * UI lock screen
 *
 * @author Pierre HUBERT
 */

/**
 * Display UI lock screen
 */
void ui_lock_show();