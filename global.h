/**
 * Global project configuration
 *
 * @author Pierre HUBERT
 */

#include <stdlib.h>
#include <stdbool.h>
#include <gtk/gtk.h>

//Log macros
#define LOG_VERBOSE(msg) {g_print("Verbose: %s\n", msg);}
#define LOG_ERROR(msg){g_print("Error: %s\n", msg);}
#define LOG_FATAL(msg) {g_print("Fatal error: %s\n", msg); exit(EXIT_FAILURE);}

//Allocation macros
#define MALLOC_OR_FATAL(var, size) {var = malloc(size); if(var == NULL){LOG_FATAL("Could not allocate memory for dynamic allocation!");}}
