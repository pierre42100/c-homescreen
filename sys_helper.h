/**
 * System ressources access helper
 *
 * @author Pierre HUBERT
 */

/**
 * Get current battery charge status
 *
 * @return Battery level / value inferior to 0 in case of failure
 */
int sys_helper_get_battery();