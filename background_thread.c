#include <pthread.h>
#include <unistd.h>
#include <time.h>

#include "background_thread.h"
#include "ui.h"
#include "config.h"
#include "sys_helper.h"
#include "edt_parser.h"

static pthread_t thread;
static bool fetched_calendar = false;


static void *thread_loop(void* data);

bool background_thread_start(){

	//Intend to create thread
	if(pthread_create(&thread, NULL, &thread_loop, NULL) < 0){
		LOG_ERROR("Could not start refresh thread!");
		return false;
	}

	return true;

}

static void *thread_loop(void *data){

	data = NULL;
	LOG_VERBOSE("Background thread started.");

	while(true){
		LOG_VERBOSE("Background thread loop.");
		g_print("Time %ld\n", time(NULL));
		usleep(1000 * 1000);

		//Fetch data and put them in a container to send back to the UI
		struct ui_data_t data;
		data.time = time(NULL);
		data.battery_charge = sys_helper_get_battery();
		ui_update_data(&data);

		//Fetch calendar information, if not done yet
		if(!fetched_calendar){
			fetched_calendar = true;

			//Request events
			char date[12];
			edt_parser_get_request_date_from_now(date, 0);
			struct edt_parser_event_t *first = edt_parser_parse(EDT_RESSOURCE_ID, date, date);

			if(first != NULL)
				edt_parser_sort(first);

			//Show calendar
			ui_show_calendar(first);

			//Free memory
			if(first != NULL)
				edt_parser_free_all(first);

		}
	}

	return NULL;
}
