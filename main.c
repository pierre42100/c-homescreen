#include <gtk/gtk.h>

#include "config.h"
#include "ui.h"
#include "app_list.h"
#include "background_thread.h"
#include "global.h"

static void activate(GtkApplication *app, gpointer user_data){

	LOG_VERBOSE("Activate app.");

	if(!app_list_init())
		LOG_FATAL("Could not initialize application list!");

	if(!ui_init(app))
		LOG_FATAL("Could not initialize UI!");
	LOG_VERBOSE("UI ready.");

	//Free memory
	app_list_free();

	if(!background_thread_start())
		LOG_FATAL("Could not start background thread!");
}

int main(int argc, char** argv){

	GtkApplication *app;
	int status;

	app = gtk_application_new(APP_ID, G_APPLICATION_FLAGS_NONE);
	g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
	status = g_application_run(G_APPLICATION(app), argc, argv);

	g_object_unref(app);

	return status;
}