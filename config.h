/**
 * Project configuration
 * 
 * @author Pierre HUBERT
 */

//Application identifier
#define APP_ID "org.communiquons.homescreen"

//File that contains battery charge location
#define BATTERY_CHARGE_LOCATION "/sys/class/power_supply/BAT0/capacity"

//Univ Lyon 1 EDT calendar ressouce ID
#define EDT_RESSOURCE_ID 41580