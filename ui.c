#include <string.h>

#include "app_list.h"
#include "ui.h"
#include "ui_lock.h"

#define NUMBER_OF_RESULTS 10

static GtkWidget *window;
static GtkWidget *date_widget;
static GtkWidget *battery_charge_widget;
static GtkWidget *calendar_widget;

static void userHitEnterOnUserInput(GtkWidget *widget, gpointer data);
static void addOptionToAppList(GtkListStore *store, char *command);

bool ui_init(GtkApplication *app) {

	//Create window
	window = gtk_application_window_new(app);
	gtk_window_set_title(GTK_WINDOW(window), "Home Screen");
	gtk_window_set_default_size(GTK_WINDOW(window), 200, 40);
	gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER_ALWAYS);

	//Add vertical container box
	GtkWidget *box;
	box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 1);
	gtk_container_add(GTK_CONTAINER(window), box);


	//Add calendar widget
	calendar_widget = gtk_label_new("Calendar is loading...");
	gtk_container_add(GTK_CONTAINER(box), calendar_widget);


	//Add date widget
	date_widget = gtk_label_new(NULL);
	gtk_container_add(GTK_CONTAINER(box), date_widget);

	//Add a label to display battery charge
	battery_charge_widget = gtk_label_new(NULL);
	gtk_container_add(GTK_CONTAINER(box), battery_charge_widget);


	//Add application search input
	GtkWidget *app_name_input;
	app_name_input = gtk_entry_new();
	gtk_entry_set_placeholder_text(GTK_ENTRY(app_name_input), "Enter application name...");
	g_signal_connect(GTK_ENTRY(app_name_input), "activate", G_CALLBACK(userHitEnterOnUserInput), NULL);
	gtk_container_add(GTK_CONTAINER(box), app_name_input);

	//Create completion object
	GtkEntryCompletion *completion;
	completion = gtk_entry_completion_new();
	gtk_entry_completion_set_inline_completion(completion, true);

	//Create list store to store available commands
	GtkListStore *ls = gtk_list_store_new(1, G_TYPE_STRING);
	app_list_append_to_list_store(ls);
	addOptionToAppList(ls, "lock");
	gtk_entry_completion_set_model(completion, GTK_TREE_MODEL(ls));

	//Apply completion object
	gtk_entry_set_completion(GTK_ENTRY(app_name_input), completion);
	gtk_entry_completion_set_text_column(completion, 0);

	//Display window
	gtk_widget_show_all(window);

	return true;
}

static void addOptionToAppList(GtkListStore *store, char *command) {

	GtkTreeIter iter;

	//Create a new entry in the row and append app name into it
	gtk_list_store_append(store, &iter);
	gtk_list_store_set(store, &iter, 0, command, -1);

}

static void userHitEnterOnUserInput(GtkWidget *widget, gpointer data){
	LOG_VERBOSE("User has hit the enter key.");

	//Execute if required a programm
	const gchar *msg = gtk_entry_get_text(GTK_ENTRY(widget));
	
	//Check if query is empty
	if(strlen(msg) < 1)
		return;

	//Check if lock screen was requested
	if(strstr(msg, "lock") != NULL){

		//Show lock screen
		ui_lock_show();

	}
	else {

		//Execute required programm
		char cmd[1000];
		sprintf(cmd, "echo \"nohup %s > /dev/null 2>&1 &\" | /bin/bash", msg);
		system(cmd);

		LOG_VERBOSE("Opened a programm.");

		gtk_widget_hide(window);
		gtk_widget_show_all(window);

	}

	//Empty entry
	gtk_entry_set_text(GTK_ENTRY(widget), "");
}

void ui_update_data(const struct ui_data_t *data){

	//Apply date time
	char date[30];
	ctime_r(&data->time, date);
	gtk_label_set_text(GTK_LABEL(date_widget), date);

	//Battery charge level (Battery: 100%)
	char battery[14];
	sprintf(battery, "Battery: %d%%", data->battery_charge);
	gtk_label_set_text(GTK_LABEL(battery_charge_widget), battery);

}

//Get the length in a string of a single event
//Format of an event 00:00 - 00:00 Summary (Location)
static int GetEventLength(struct edt_parser_event_t *event){
	return 17 + strlen(event->summary) + strlen(event->location);
}

void ui_show_calendar(struct edt_parser_event_t * const firstEvent){

	//Check if an error occurred
	if(firstEvent == NULL){
		gtk_label_set_text(GTK_LABEL(calendar_widget), "Could not fetch calendar!");
		return;
	}


	//Process the list of events and write it into a string
	struct edt_parser_event_t *curr_event = firstEvent;
	
	//Determine the size of the string to generate
	int events_length = 0;
	while(curr_event != NULL){
		events_length += GetEventLength(curr_event) + 1 /* +1 for line break */;
		curr_event = curr_event->previous;
	}
	events_length++;

	//Allocate memory for the event
	char *event_string;
	MALLOC_OR_FATAL(event_string, sizeof(char)*events_length);
	*event_string = '\0';
	
	curr_event = firstEvent; //Seek pointer to the begining :)
	while(curr_event != NULL){
		sprintf(
			strchr(event_string, '\0'),
			"%02d:%02d - %02d:%02d %s (%s)\n",
			curr_event->date_start.hour,
			curr_event->date_start.minute,
			curr_event->date_end.hour,
			curr_event->date_end.minute,
			curr_event->summary,
			curr_event->location
		);

		curr_event = curr_event->previous;
	}

	//Display the calendar
	gtk_label_set_text(GTK_LABEL(calendar_widget), event_string);

	//Free memory
	free(event_string);
}
