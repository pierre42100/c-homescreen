/**
 * Application list management
 *
 * @author Pierre HUBERT
 */

#include "global.h"

/**
 * Initialize application list 
 *
 * @return TRUE in case of success / FALSE else
 */
bool app_list_init();

/**
 * Make a search into applications list
 *
 * @param query Search query to execute
 * @param **results The array to fill with results (Note : The results MUST NOT BE MODIFIED!!!)
 * @param max_results The number of maximal results to return
 * @param *number_results Int that will be filled with the number of actual results
 */
void app_list_search(const char *query, char** results, int max_results, int *number_results);

/**
 * Append the list of application to a GtkListStore object
 *
 * @param store The store where entries will be applied
 */
void app_list_append_to_list_store(GtkListStore *store);

/**
 * Free memory by unloading applications list
 */
void app_list_free();